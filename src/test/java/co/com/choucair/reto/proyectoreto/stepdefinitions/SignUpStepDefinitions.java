package co.com.choucair.reto.proyectoreto.stepdefinitions;

import co.com.choucair.reto.proyectoreto.model.UtestData;
import co.com.choucair.reto.proyectoreto.questions.Answer;
import co.com.choucair.reto.proyectoreto.tasks.*;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.EventualConsequence;
import net.serenitybdd.screenplay.GivenWhenThen;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

import java.util.List;

public class SignUpStepDefinitions {

    @Given("^than (.*) wants to join the uTest community$")
    public void thanBrandomWantsToJoinTheUTestCommunity(String actorName) {
        theActorCalled(actorName).wasAbleTo(OpenUp.thePage(), Join.onThePage());
    }

    @When("^he fill out the registration form on the uTest web page\\.$")
    public void heFillOutTheRegistrationFormOnTheUTestWebPage(List<UtestData> data) {
        theActorInTheSpotlight().attemptsTo(FillSignUpPersonalForm.the(data.get(0)),
                                                    FillSignUpLocationForm.the(data.get(0)),
                                                    FillSignUpDevicesForm.the(data.get(0)),
                                                    FillSignUpLastStepForm.the(data.get(0)));
    }

    @Then("^he should see the confirmation button$")
    public void heShouldSeeTheConfirmationButton(List<UtestData> data) {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(
                Answer.toThe(data.get(0).getButtonText())) );
    }
}


