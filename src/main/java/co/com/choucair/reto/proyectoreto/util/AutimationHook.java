package co.com.choucair.reto.proyectoreto.util;

import cucumber.api.java.Before;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

public class AutimationHook {

    @Before
    public void setUp(){
        OnStage.setTheStage(new OnlineCast());
    }

}
