package co.com.choucair.reto.proyectoreto.exceptions;

public class CaptchaException extends AssertionError {

    public static final String CAPTCHA_ELEMENT_FOUND = "Se ha activado el Captcha";
    public CaptchaException(String message, Throwable cause) {
        super(message, cause);
    }
}
